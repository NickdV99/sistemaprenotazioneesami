package sistemaprenotazione.control;
import java.util.ArrayList;
import java.util.Scanner;

import sistemaprenotazione.entity.AddettoSegreteria;
import sistemaprenotazione.entity.Corso;
import sistemaprenotazione.entity.Docente;
import sistemaprenotazione.entity.Esame;
import sistemaprenotazione.entity.Studente;

public class GestoreAppelli {


	public GestoreAppelli() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {

		GestoreAppelli ga = new GestoreAppelli(); 
		AddettoSegreteria segr = new AddettoSegreteria(); 
		Docente doce = new Docente("Natella"); 
		Studente stud = new Studente("N46004106", "66666"); 
		Esame esame = new Esame(); 

		//NOTA : Per scopi dimostrativi la creazione del corse è stata gestita da main, mentre per la creazione 
		//		 dell'appello e la prenotazione dello studente gli input sono dati da tastiera. 
		//		Per lo stesso fine parte dei controlli sono stati fatti con un if-else, parte tramite eccezioni 
		
		
		// Creazione corso con controllo if-else 
		
		if(ga.creaCorso(segr,"ID01", "Ing Software")) {
			// creazione avvenuta con successo
		}else {
			//creazione fallita
		}

		
		//Inserimento corso corretto

		Corso corsoInserito = segr.getCorso("ID01");

		// In questo caso il corsoInserito non potrà mai essere null in quanto dato da main 
		if(corsoInserito == null) {
			//Errore...
		}

		
		//Creazione esame
		
		ga.creaEsame(doce, corsoInserito);

		
		//Selezionamento esame con gestione dell'eccezione
		
		boolean selezionamento = true;
		while(selezionamento) {
			esame = ga.sceltaEsame(corsoInserito.getListaEsami());

			try {
				System.out.println("");
				ga.prenotazioneEsame(stud, esame);
				System.out.println("Prenotazione avvenuta con successo!");
			} catch (BookingExeption e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage()); 
			}

			selezionamento = ga.selezionaAltroCorso();
		}

		
		//Visualizzazione Prenotati 
		
		ga.visualizzaPrenotati(esame.getIscritti());


	}



	public boolean creaCorso(AddettoSegreteria as, String id, String nomeCorso) {

		return as.creaCorso(id, nomeCorso); 


	}

	public void creaEsame(Docente doc, Corso nomeCorso) {

		doc.finalizzaAppello(nomeCorso);
	}


	public void visualizzazioneListaEsami(ArrayList<Esame> lista) {

		System.out.println(); 
		for (Esame es: lista) {
			System.out.println( (lista.indexOf(es) + 1) + ". " +  es.toString());
		}

	}


	public Esame sceltaEsame(ArrayList<Esame> lista) {


		System.out.print("---Lista Esami---");
		this.visualizzazioneListaEsami(lista);

		System.out.println("");
		System.out.println("Seleziona Esame"); 
		Scanner tastiera = new Scanner(System.in); 
		int scelta = tastiera.nextInt(); 
		while (!(scelta-1<lista.size() && scelta>0)){
			System.out.print("Esame non esistente. Riprova:" );
			scelta = tastiera.nextInt();
			System.out.println();
		}
		System.out.println("Esame selezionato correttamente.");

		return lista.get(scelta-1); 

	}

	public void prenotazioneEsame(Studente stud, Esame esame) throws BookingExeption {



		System.out.println("");
		System.out.println("CONFERMA la selezione di questo esame? (0=no, 1=si)"); 
		Scanner tastiera = new Scanner(System.in); 
		int scelta = tastiera.nextInt(); 
		while (scelta != 0 && scelta != 1){
			System.out.print("Seleziona la giusta opzione!");
			scelta = tastiera.nextInt();
			System.out.println();
		}
		
		if(scelta == 1){
			System.out.println("...Prenotazione in corso...");
			stud.prenotazioneEsame(esame);
		}else {
			// exit 
			throw new BookingExeption("OK, Prenotazione non effettuata");
			
		}
	}

	public boolean selezionaAltroCorso() {

		System.out.println("Vuoi selezionare UN ALTRO esame? (0=no, 1=si)");
		Scanner tastiera = new Scanner(System.in); 
		int scelta = tastiera.nextInt(); 
		while (scelta != 0 && scelta != 1){
			System.out.print("Seleziona la giusta opzione!" );
			scelta = tastiera.nextInt();
			System.out.println();
		}
		if(scelta == 1){
			return true;
		}else {
			return false;
		}


	}



	public void visualizzaPrenotati(ArrayList<Studente> lista) {

		System.out.print("---Lista Prenotati---");
		System.out.println(); 
		for (Studente st : lista) {
			System.out.println( (lista.indexOf(st) + 1) + ". " + st.toString());
		}

	}




}
