package sistemaprenotazione.entity;

import java.util.*;

import sistemaprenotazione.control.BookingExeption;

public class Esame {


	private String id; 
	private enum modalità {scritto,orale,al_calcolatore}
	private Data data; 

	private ArrayList<Studente> iscritti = new ArrayList<Studente>();
	

	public Esame() {
		// TODO Auto-generated constructor stub
	}
	
	public Esame(Data data) {
		this.data = data;
		
	}
	
	
	public Data getData() {
		return data;
	}


	public void setData(Data data) {
		this.data = data;
	}


	public ArrayList<Studente> getIscritti() {
		return iscritti;
	}
	
	public void prenotaStudentePerEsame(Studente studente) throws BookingExeption {
		if(!iscritti.contains(studente)) {
			iscritti.add(studente);
		}else {
			throw new BookingExeption("Error! Lo studente è già prenotato per questo esame!");
		}
		
	}

	public String getId() {
		return id;
	}


	@Override
	public String toString() {
		return "Esame in Data: " + data + "";
	}

	
}
