package sistemaprenotazione.entity;

import java.util.ArrayList;

import sistemaprenotazione.control.BookingExeption;

public class Studente {

	private String matricola; 
	private String pin; 
	
	public Studente() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	 public Studente(String matricola, String pin) {

		this.matricola = matricola;
		this.pin = pin;
	}



	public void prenotazioneEsame(Esame esame) throws BookingExeption {
		 esame.prenotaStudentePerEsame(this); 
	 }


	public String getMatricola() {
		return matricola;
	}



	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	
	@Override
	public String toString() {
		return "Studente [matricola=" + matricola + "]";
	}
	
	
	
	
}
