package sistemaprenotazione.entity;
import java.util.ArrayList;

public class Corso {

	private String id; 
	private String nome;
	
	ArrayList<Esame> listaEsami = new ArrayList<Esame>(); 
	

	public Corso() {	
	}

	public Corso(String nome) {
		this.nome = nome;
	}

		
	public Corso(String id, String nome) {
		this.nome = nome;
		this.id = id; 
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Corso other = (Corso) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (listaEsami == null) {
			if (other.listaEsami != null)
				return false;
		} else if (!listaEsami.equals(other.listaEsami))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		return true;
	}

	public ArrayList<Esame> getListaEsami() {
		return listaEsami;
	}


	
	
}
