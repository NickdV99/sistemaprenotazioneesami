package sistemaprenotazione.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddettoSegreteria {

	Map<String, Corso> listaCorsi = new HashMap<>(); 

	public AddettoSegreteria() {
			
	}
	
	
	public boolean creaCorso(String id, String nome) {		
		Corso c = new Corso(id, nome);
		
		if(!listaCorsi.containsKey(id)){
			listaCorsi.put(id, c);
			return true;
		}else {
			return false;
		}

		
	}
	
	public Corso getCorso(String id){
		
		return listaCorsi.get(id);
		
	}
	
	
	public boolean safeAdd(ArrayList<Corso> lista, String id, String nome) {
		
		for (Corso cc : lista) {
			if (cc.getId().equals(id)) {
				return false;
			}
		}
		return true;
	}
	
	
	
}
