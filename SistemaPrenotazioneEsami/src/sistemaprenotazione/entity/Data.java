package sistemaprenotazione.entity;

public class Data {

	
	private int giorno; 
	private int mese; 
	private int anno; 
	
	public Data(int giorno, int mese, int anno) {
		
		this.giorno = giorno;
		this.mese = mese;
		this.anno = anno;
		
	}

	public int getGiorno() {
		return giorno;
	}

	public void setGiorno(int giorno) {
		this.giorno = giorno;
	}

	public int getMese() {
		return mese;
	}

	public void setMese(int mese) {
		this.mese = mese;
	}

	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public Data() {
		// TODO Auto-generated constructor stub
	}

	public Boolean isValid() {
		return (this.giorno <32 && this.giorno >0 && this.mese <13 && this.mese >0 && this.anno >0); 
	}
	

	public Boolean isNotEmpty() {
		return !this.equals(new Data());
	}

	@Override
	public String toString() {
		return "[giorno=" + giorno + ", mese=" + mese + ", anno=" + anno + "]";
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Data other = (Data) obj;
		if (anno != other.anno)
			return false;
		if (giorno != other.giorno)
			return false;
		if (mese != other.mese)
			return false;
		return true;
	}
	
}

