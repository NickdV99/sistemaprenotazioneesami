package sistemaprenotazione.entity;
import java.util.Scanner;

public class Docente {

	private String cognome; 

	public Docente() {
		// TODO Auto-generated constructor stub
	}


	public Docente(String cognome) {

		this.cognome = cognome;
	}

	public Esame creaAppello() {

		System.out.println("---Creazione Appello---");
		Data data = new Data(); 
		Scanner tastiera = new Scanner(System.in); 


		while(!(data.isNotEmpty() && data.isValid())){
			System.out.print("Inserisci Giorno: ");
			int scelta = tastiera.nextInt();
			data.setGiorno(scelta);
			System.out.println();
			System.out.print("Inserisci Mese: ");
			scelta = tastiera.nextInt();
			data.setMese(scelta);
			System.out.println();
			System.out.print("Inserisci Anno: ");
			scelta = tastiera.nextInt();
			data.setAnno(scelta);
			System.out.println();
			if (!data.isValid()) {
				System.out.println("Data inserita non valida. Reinserire data");}
		}



		return new Esame(data);
	}

	public void finalizzaAppello(Corso c) {
		Esame e = this.creaAppello();
		c.listaEsami.add(e);
	}

}
