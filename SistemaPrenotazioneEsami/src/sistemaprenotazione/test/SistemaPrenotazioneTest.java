package sistemaprenotazione.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import sistemaprenotazione.control.BookingExeption;
import sistemaprenotazione.control.GestoreAppelli;
import sistemaprenotazione.entity.*;


public class SistemaPrenotazioneTest {

	ArrayList<Corso> listaCorsi; 
	ArrayList<Studente> listaPrenotati; 
	ArrayList<Esame> listaEsami; 
	GestoreAppelli ga;
	AddettoSegreteria segr; 
	Docente doce; 
	Studente stud , stud1; 
	Corso corso, corso1; 
	Esame esame; 

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {


	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {

		ga = new GestoreAppelli();
		segr = new AddettoSegreteria();
		stud = new Studente("N46004106", "66666");
		stud1 = new Studente("N36003578", "55555");
		doce = new Docente("Natella");
		corso = new Corso("ID01", "Ing Software");
		corso1 = new Corso("ID02", "IA");
		listaCorsi = new ArrayList<Corso>();
		listaPrenotati = new ArrayList<Studente>(); 
		listaEsami = new ArrayList<Esame>();
		esame = new Esame(); 
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void test01() {

		boolean status1 = ga.creaCorso(segr,"ID01", "Ing Software");

		assertEquals(true, status1);

	}

	@Test
	public void test02() {

		boolean status1 = ga.creaCorso(segr,"ID01", "Ing Software");
		boolean status2 = ga.creaCorso(segr,"ID01", "Ing Software");


		assertEquals(true, status1);
		assertEquals(false, status2);


	}

	@Test
	public void test03() {

		boolean status1 = ga.creaCorso(segr,"ID01", "Ing Software");
		boolean status2 = ga.creaCorso(segr,"ID02", "Ing Software");


		assertEquals(true, status1);
		assertEquals(true, status2);

	}


	@Test
	public void test04() {

		ga.creaEsame(doce, corso);
		ga.visualizzazioneListaEsami(corso.getListaEsami());

	}

	@Test
	public void test05() {

		ga.creaEsame(doce, corso);
		ga.creaEsame(doce, corso);

	}


	@Test 
	public void test06() throws BookingExeption {

		ga.creaEsame(doce, corso);

		esame = ga.sceltaEsame(corso.getListaEsami());

		ga.prenotazioneEsame(stud, esame);
		
		System.out.println("Prenotazione avvenuta con successo!");


	}


	@Test
	public void test07() {

		ga.creaEsame(doce, corso);

		boolean selezionamento = true;
		while(selezionamento) {
			esame = ga.sceltaEsame(corso.getListaEsami());

			try {
				System.out.println("");
				ga.prenotazioneEsame(stud, esame);
				System.out.println("Prenotazione avvenuta con successo!");
			} catch (BookingExeption e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage()); 
			}

			selezionamento = ga.selezionaAltroCorso();
		}

	}
	@Test 
	public void test08() throws BookingExeption {

		ga.creaEsame(doce, corso);
		ga.creaEsame(doce, corso);
		boolean selezionamento = true;
		while(selezionamento) {
			esame = ga.sceltaEsame(corso.getListaEsami());

			try {
				System.out.println("");
				ga.prenotazioneEsame(stud, esame);
				System.out.println("Prenotazione avvenuta con successo!");
			} catch (BookingExeption e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage()); 
			}

			selezionamento = ga.selezionaAltroCorso();
		}

	}

	@Test
	public void test09() {
		
		ga.creaEsame(doce, corso);
		ga.visualizzazioneListaEsami(corso.getListaEsami());
	}



	@Test
	public void test10() {
		ga.creaEsame(doce, corso);
		ga.creaEsame(doce, corso);
		ga.visualizzazioneListaEsami(corso.getListaEsami());
	}


	@Test
	public void test11() {

		ga.visualizzazioneListaEsami(corso.getListaEsami());
	}



	@Test
	public void test12() throws BookingExeption {
	
		stud.prenotazioneEsame(esame);
		ga.visualizzaPrenotati(esame.getIscritti()); 
	}

	@Test
	public void test13() throws BookingExeption {

		stud.prenotazioneEsame(esame);
		stud1.prenotazioneEsame(esame);
		ga.visualizzaPrenotati(esame.getIscritti());
	}


	@Test
	public void test14() {

		ga.visualizzaPrenotati(esame.getIscritti());
	}




}
